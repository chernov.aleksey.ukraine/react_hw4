import React from "react";
import Card from "../Card/Card";
import "./ItemList.scss";
import { useSelector, useDispatch } from "react-redux";

const ItemList = () => {
  
  const array = useSelector((store) => store.header.array);
   
  



    return (
      <div className="itemcontainer">
        {array.map(({ name, art, color, price, url, isFavorite, isCart }) => (
          <Card
            key={art}
            name={name}
            art={art}
            color={color}
            price={price}
            url={url}
            isFavorite={isFavorite}
            isCart={isCart}
            
            // moveFromCart={moveFromCart}
            
            // chooseItem={chooseItem}
          />
        ))}
      </div>
    );
  
}
ItemList.propTypes = {};
export default ItemList;
