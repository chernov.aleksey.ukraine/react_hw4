import "./App.scss";
import Header from "./Components/Header/Header";
import { BrowserRouter } from "react-router-dom";
import AppRoutes from "./Routes";
import { Provider } from "react-redux";
import store from "./store";


const App = () => {
  // const [array, setArray] = useState([]);
  // const [isModalOpen, setIsModalOpen] = useState(false);
  // const [chosenItem, setChosenItem] = useState({});

  // const addToCart = (art) => {
  //   setArray((current) => {
  //     const array = [...current];
  //     const index = array.findIndex((el) => el.art === art);

  //     if (index !== -1) {
  //       array[index].isCart = true;
  //     }
  //     localStorage.setItem("array", JSON.stringify(array));
  //     return array;
  //   });
  // };

  
  // const chooseItem = (value) => {
  //   setChosenItem(value);
  // };



 

  // const moveFromCart = (art) => {
  //   setArray((current) => {
  //     const array = [...current];
  //     const index = array.findIndex((el) => el.art === art);
  //     if (index !== -1) {
  //       array[index].isCart = false;
  //     }
  //     localStorage.setItem("array", JSON.stringify(array));
  //     return array;
  //   });
  // };
  // 
  // 



  return (
    <Provider store={store}>
      <BrowserRouter>
        <div className="App">
          <Header />
          <AppRoutes />
          
        </div>
      </BrowserRouter>
    </Provider>
  );
};

export default App;
