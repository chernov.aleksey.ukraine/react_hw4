import { combineReducers } from "redux";
import headerReducer from "./header/reducer";
// import counterReducer from "./counter/reducer";
// import letterReducer from "./letter/reducer";

const appReducer = combineReducers({
  header: headerReducer,
//   letter: letterReducer,
//   cats: catsReducer,
});

export default appReducer;
