import React from "react";
import CartList from "../../Components/CartList/CartList";
import Modalwindow from "../../Components/Modalwindow/Modalwindow";


const Cart = () => {
  

  return (
    <div>
      <CartList />
      <Modalwindow
        operation={"move"}
        headertext={"Removing item to the cart."}
        maintext1={"Do you want to remove "}
        maintext2={" from the cart?"}
      />
    </div>
  );
};

export default Cart;
