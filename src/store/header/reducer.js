import {
  GET_ARRAY,
  IS_MODAL_OPEN,
  ADD_TO_FAVORITE,
  MOVE_FROM_FAVORITE,
  CHOSEN_TO_OPERATE,
  ADD_TO_CART,
  MOVE_FROM_CART,
} from "./actions";

const saveChangesToLS = (value) => {
  localStorage.setItem("array", JSON.stringify(value));
};



const initialValue = {
  array: [],
  isModalOpen: false,
  chosenToOperate: {}, 
};
const headerReducer = (state = initialValue, action) => {
  switch (action.type) {
    case GET_ARRAY: {
      return { ...state, array: action.payload };
    }
    case IS_MODAL_OPEN: {
      return { ...state, isModalOpen: action.payload };
    }

    case ADD_TO_FAVORITE: {
      const newArray = [...state.array];
      console.log(newArray);
      const index = newArray.findIndex((el) => el.art === action.payload);
      if (index !== -1) {
        newArray[index].isFavorite = true;
      }
      saveChangesToLS(newArray);
      return { ...state, array: newArray };
    }
    case MOVE_FROM_FAVORITE: {
      const newArray = [...state.array];
      console.log(newArray);
      const index = newArray.findIndex((el) => el.art === action.payload);
      if (index !== -1) {
        newArray[index].isFavorite = false;
      }
      saveChangesToLS(newArray);
      return { ...state, array: newArray };
    }
    case CHOSEN_TO_OPERATE: {
      return { ...state, chosenToOperate: action.payload };
    }
    case ADD_TO_CART: {
      const newArray = [...state.array];
      console.log(newArray);
      const index = newArray.findIndex((el) => el.art === action.payload);
      if (index !== -1) {
        newArray[index].isCart = true;
      }
      saveChangesToLS(newArray);
      return { ...state, array: newArray };
    }
    case MOVE_FROM_CART: {
      const newArray = [...state.array];
      console.log(newArray);
      const index = newArray.findIndex((el) => el.art === action.payload);
      if (index !== -1) {
        newArray[index].isCart = false;
      }
      saveChangesToLS(newArray);
      return { ...state, array: newArray };
    }
    default: {
      return state;
    }
  }
};

export default headerReducer;
