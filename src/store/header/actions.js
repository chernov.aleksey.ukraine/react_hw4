export const GET_ARRAY = "GET_ARRAY";
export const IS_MODAL_OPEN = "IS_MODAL_OPEN";
export const ADD_TO_FAVORITE = "ADD_TO_FAVORITE";
export const MOVE_FROM_FAVORITE = "MOVE_FROM_FAVORITE";
export const CHOSEN_TO_OPERATE = "CHOSEN_TO_OPERATE";
export const ADD_TO_CART = "ADD_TO_CART";
export const MOVE_FROM_CART = "MOVE_FROM_CART";
