import React, { useEffect } from "react";
import { NavLink } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { getArrayAC } from "../../store/header/actionCreators";

let counter = 0;
const Navigation = () => {

  const favorCount = ({array}) => {
    counter = 0;
    array.forEach((el) => {
      if (el.isFavorite) {
        counter += 1;
      }
    });
      return counter;
  };

  const cartCount = ({array}) => {
    counter = 0;
    array.forEach((el) => {
      if (el.isCart) {
        counter += 1;
      }
    });
    return counter;
  };

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getArrayAC());
  }, []);

  const array = useSelector((store) => store.header.array);

  return (
    <nav>
      <NavLink to="/">Home Page</NavLink>

      <NavLink to="/favorite">Favorites( {favorCount({ array })} )</NavLink>

      <NavLink to="/cart">Cart( {cartCount({ array })} )</NavLink>
    </nav>
  );
};
Navigation.propTypes = {};
export default Navigation;
